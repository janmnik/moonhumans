using System.ComponentModel.DataAnnotations;


namespace MoonHumans.Models
{
    public class Alien{
         
        [Key]
        public int ID {get;set;}
        [Required]
        public int Value {get;set;}
        [Required]
        public int  Color {get;set;}

        public string BodyType {get;set;}
         public string HeadType {get;set;}

        public string EarsType {get;set;}

        public string EyesType {get;set;}

        public string MouthType {get;set;}        

        public override string ToString(){
            return $"{Value} {Color}";

        }
    }

}