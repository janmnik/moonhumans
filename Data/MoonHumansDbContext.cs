using Microsoft.EntityFrameworkCore;
using MoonHumans.Models;

namespace MoonHumans.Data{
    public class MoonHumansDbContext : DbContext{
        public DbSet<Alien> Aliens;
        public MoonHumansDbContext(DbContextOptions<MoonHumansDbContext> options):base(options)
        {
           //DBInitializer.Initialize(this);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder){
        }
    }
}